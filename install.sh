#! /bin/bash
#make sh executable, copy it to the $PATH
chmod +x mneme.sh
[[ -d $HOME/.local/bin/ ]]&&cp mneme.sh $HOME/.local/bin/&&INSTALL_MESSAGE="The script was copied to\n\e[33m $HOME/.local/bin/\e[m\nProvided that this directory is included in the '\$PATH', the user can run the script with\n\e[33m$ mneme.sh\e[m\nfrom any directory.\nAlternatively, the script can be run with\n\e[33m$ ./mneme.sh\e[m\nfrom the mneme/ directory."||INSTALL_MESSAGE="The script has been made executable and the user can run it with:\n\e[33m$ ./mneme.sh\e[m\nfrom the mneme/ directory."
# create the necessary directories and files:
mkdir -p $HOME/.local/share/mneme $HOME/.config/mneme/
cp  mneme.png $HOME/.local/share/mneme/

echo -e "#Acceptable level values:easy, medium, hard, harder, hardest
LEVEL=easy

#Acceptable theme values, nerd-fonts dependent:(animals, linux, zodiac, symbols, objects, weather), nerd-fonts non-dependent:(letters, numeric).
CHARACTER_THEME=animals

#Acceptable notification toggle values: yes / no
NOTIFICATION_TOGGLE=yes

#Acceptable notification toggle values: yes / no. This can also be controlled while playing.
CHEATSHEET_TOGGLE=yes

#Text editor to open config file
PREFERRED_EDITOR=${EDITOR-nano}

#Key binding to open squares. Spacebar is hardcoded, also. Non acceptable values: Upper-case,q,i, arrow keys, navigation key-bindings.
OPEN_BIND=f

#Acceptable values: vim, aswd. Arrow keys are hardcoded and work in all options.
NAVIGATION_KEYS=vim">$HOME/.config/mneme/mneme.config
echo -e "$INSTALL_MESSAGE"
