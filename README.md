# mneme

**mneme** is a version of the classical memory game, that the user can play in a terminal window. It is a script written in Bash.

![main menu](screenshots/main_menu.png){width=300}

The user in order to win has to open all squares in matching pairs. The squares have to match not only in **symbol**, but in **color** as well.

If the squares match, the squares stay open. If not, they close again. Thus, the player has to remember the position of each particularly colored symbol in the matrix.


In the end, as mentioned, the user wins where all the squares have opened and matched.

![win](screenshots/win.png){width=300}



---

## Usage


Running the script, the user  through the main menu, can:

- Start a new game

![new game](screenshots/new.png){width=300}

- Configure the parameters of the game according to taste

![edit preferences](screenshots/edit.png){width=300}

- Check out statistics

![top ten](screenshots/hiscore.png){width=300}

 - Just exit the game

![notification](screenshots/notification.png){height=50}

---
## Playing

The user can move the cursor with `h,j,k,l` (vim keys), or the arrow keys.

- As soon as the user hits the `open key` (`f` or `spacebar` by default), the cursor square will open. Opening another square, the pair of squares can either match, or not.


![no match](screenshots/no_match.png){width=100}
![match](screenshots/match.png){width=100} 



 - While playing the user can also hide or show the keybinding cheatsheet(`i,I`)

 ![info on](screenshots/info_on.png){width=150}
 ![info on](screenshots/info_off.png){width=150}

- If the user gets tired, quitting is always an option.

![quit](screenshots/quit.png){width=300}



---
## Configuring

As mentioned above, the games parameters can be configured within the game from the `main menu`, or by editing the `$HOME/.config/mneme/mneme.config` file.

|n|Variable|Explanation| Acceptable Values|Default Value|
|---|---|---|---|---|
|1| LEVEL|Difficulty of the game-size of matrix|easy (4x4) medium (6x6) hard (8x8) harder (10x10) hardest (12x12)|easy|
|2|CHARACTER_THEME|Character theme used in the grid|**nerd-fonts dependent:** animals, linux, zodiac, symbols, objects, weather. **nerd-fonts non-dependent:** letters, numeric|animals|
|3|NOTIFICATION_TOGGLE|Show Notifications while playing|yes / no| yes|
|4|CHEATSHEET_TOGGLE|Show keybinding cheatsheet by default (can be also toggled while playing)|yes / no| yes|
|5|PREFERRED_EDITOR |Editor to be used to open the config file|Any gui or tui text editor|`$EDITOR`\|\|`nano`|
|6|OPEN_BIND|Key binding to open squares. Spacebar is hardcoded, also.|**CAUTION**: NON-ACCEPTABLE VALUES:`Upper-case`,`q`,`i`, `arrow keys`, `navigation key-bindings`.|f|
|7|NAVIGATION_KEYS|Keys to navigate in the grid. Arrow keys are hardcoded and work in all options.| vim(`hjkl`), aswd |vim|


- If the configuration file is not properly loaded, the game will begin with hardcoded default values.


---
### Level of difficulty

The greater the matrix, the harder the puzzle: Choose between **easy** (4x4), **medium** (6x6), **hard** (8x8), **harder** (10x10), **hardest** (12x12)

![easy](screenshots/easy.png){width=80}
![medium](screenshots/medium.png){width=120}
![hard](screenshots/hard.png){width=160}
![harder](screenshots/harder.png){width=200}
![hardest](screenshots/hardest.png){width=240}
---
### Themes

The user can use the character theme according to their taste (or system)

- Non nerd-fonts dependent 

 ![letters](screenshots/letters.png){width=100} 
 ![numerical](screenshots/numeric.png){width=100} 

- Nerd-fonts dependent

 ![animals](screenshots/animals.png){width=100} 
 ![objects](screenshots/objects.png){width=100} 
 ![symbols](screenshots/symbols.png){width=100} 
 ![zodiac](screenshots/zodiac.png){width=100} 
 ![linux](screenshots/linux.png){width=100} 
 ![weather](screenshots/weather.png){width=100} 

---

## Dependencies

As mentined above, the only dependency are [Nerd-fonts](https://www.nerdfonts.com/cheat-sheet), in order for the mine and flag characters to be rendered. **However**,  Nerd-fonts are not necessary, as long as non-Nerd-fonts themes are used instead (*numeric, letters*).

In order to install Nerd-fonts, download Ubuntu-Nerd-fonts (or some other font adequate for your system) from [https://www.nerdfonts.com/cheat-sheet](https://www.nerdfonts.com/cheat-sheet), and copy the exported `ttf`files to `~/.local/share/fonts/`.

Some systems may work differently, in order to install these fonts, the user may need to consult the respective manuals or fora.

---
## Install

- Clone to repository, and move to the `mneme` directory:

```
git clone https://gitlab.com/christosangel/mneme.git&&cd mneme/
```
- Make `install.sh` executable, then run it

```
chmod +x install.sh&&./install.sh
```
You are good to go. The script has been copied to `~/.local/bin/`, if this directory is in the system's `$PATH`, You can run it with

```
mneme.sh
```
otherwise, from the `mneme/` directory:

```
./mneme.sh
```

---

**Enjoy!**
